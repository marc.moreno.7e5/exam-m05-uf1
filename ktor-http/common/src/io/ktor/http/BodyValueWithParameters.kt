/*
* Copyright 2014-2021 JetBrains s.r.o and contributors. Use of this source code is governed by the Apache 2.0 license.
*/

//io.ktor.http

//o.ktor.util.*
//otlin.native.concurrent.*

//rator symbols listed in RFC https://tools.ietf.org/html/rfc2616#section-2.2 */
//mmutable
//val HeaderFieldValueSeparators =
//f('(', ')', '<', '>', '@', ',', ';', ':', '\\', '\"', '/', '[', ']', '?', '=', '{', '}', ' ', '\t', '\n', '\r')

//
//sents a header value that consist of [content] followed by [parameters].
//l for headers such as `Content-Type`, `Content-Disposition` and so on.
//
//erty content header's content without parameters
//erty parameters
//
//bstract class HeaderValueWithParameters(
//ected val content: String,
//ic val parameters: List<HeaderValueParam> = emptyList()
//

//
//he first value for the parameter with [name] comparing case-insensitively or `null` if no such parameters found
//
//ic fun parameter(name: String): String? =
//parameters.firstOrNull { it.name.equals(name, ignoreCase = true) }?.value

//ride fun toString(): String = when {
//parameters.isEmpty() -> content
//else -> {
//    val size = content.length + parameters.sumOf { it.name.length + it.value.length + 3 }
//    StringBuilder(size).apply {
//        append(content)
//        for (element in parameters) {
//            val (name, value) = element
//            append("; ")
//            append(name)
//            append("=")
//            value.escapeIfNeededTo(this)
//        }
//    }.toString()
//}
//

//ic companion object {
///**
// * Parse header with parameter and pass it to [init] function to instantiate particular type
// */
//public inline fun <R> parse(value: String, init: (String, List<HeaderValueParam>) -> R): R {
//    val headerValue = parseHeaderValue(value).single()
//    return init(headerValue.value, headerValue.params)
//}
//
//

//
//d formatted header value to the builder
//
//un StringValuesBuilder.append(name: String, value: HeaderValueWithParameters) {
//nd(name, value.toString())
//

//
//e using double quotes if needed or keep as is if no dangerous strings found
//
//un String.escapeIfNeeded(): String = when {
//kNeedEscape() -> quote()
// -> this
//

//s("NOTHING_TO_INLINE")
//inline fun String.escapeIfNeededTo(out: StringBuilder) {
// {
//checkNeedEscape() -> out.append(quote())
//else -> out.append(this)
//
//

//fun String.checkNeedEscape(): Boolean {
//isEmpty()) return true
//isQuoted()) return false

//(index in 0 until length) {
//if (HeaderFieldValueSeparators.contains(this[index])) return true
//

//rn false
//

//fun String.isQuoted(): Boolean {
//length < 2) {
//return false
//
//first() != '"' || last() != '"') {
//return false
//
//startIndex = 1
//
//val index = indexOf('"', startIndex)
//if (index == lastIndex) {
//    break
//}

//var slashesCount = 0
//var slashIndex = index - 1
//while (this[slashIndex] == '\\') {
//    slashesCount++
//    slashIndex--
//}
//if (slashesCount % 2 == 0) {
//    return false
//}

//startIndex = index + 1
//ile (startIndex < length)

//rn true
//

//
//e string using double quotes
//
//un String.quote(): String = buildString { this@quote.quoteTo(this) }

//fun String.quoteTo(out: StringBuilder) {
//append("\"")
//(i in 0 until length) {
//when (val ch = this[i]) {
//    '\\' -> out.append("\\\\")
//    '\n' -> out.append("\\n")
//    '\r' -> out.append("\\r")
//    '\t' -> out.append("\\t")
//    '\"' -> out.append("\\\"")
//    else -> out.append(ch)
//}
//
//append("\"")
//
